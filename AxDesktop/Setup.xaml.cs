﻿using CairoDesktop.SupportingClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace AxDesktop
{
    /// <summary>
    /// Interaction logic for Setup.xaml
    /// </summary>
    public partial class Setup : Window
    {
        public Setup()
        {
            InitializeComponent();

            this.Width = AppBarHelper.PrimaryMonitorSize.Width;
            this.Height = AppBarHelper.PrimaryMonitorSize.Height + 44;

            initializeClock();
        }

        private void initializeClock()
        {
            // initial display
            clock_Tick();

            // Create our timer for clock
            DispatcherTimer clock = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Background, delegate
            {
                clock_Tick();
            }, this.Dispatcher);
        }

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool ExitWindowsEx(uint uFlags, uint dwReason);

        public static bool WindowsLogOff()
        {
            return ExitWindowsEx(0, 0);
        }

        int i = 0;
        private void clock_Tick()
        {
           
            if(i == 20)
            {
                status.Content = "Preparing Linux Kernel...";
            }
            if (i == 30)
            {
                status.Content = "Restarting...";
                File.Create("C:/AxOS/setup.ax");
            }
            if (i == 40)
            {
                ExitWindowsEx(0 | 0x00000004, 0);

            }
            i++;
        }
    }
}
