﻿using CairoExplorer;
using IWshRuntimeLibrary;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CairoDesktop
{
    /// <summary>
    /// Interaction logic for StartMenu.xaml
    /// </summary>
    public partial class StartMenu : Window
    {
        private ObservableCollection<StartMenuItem> StartMenuItems;

        public StartMenu()
        {
            InitializeComponent();
            StartMenuItems = new ObservableCollection<StartMenuItem>();
            getStartmenu();
        }

        public void getStartmenu()
        {

            /*StreamReader file = System.IO.File.OpenText(@"C:\AxOS\StartMenu.json");
            string st = file.ReadToEnd();
            Icon img = null;

            foreach (var i in InstalledApplications.FromJson(st).InstalledApplicationsInstalledApplications)
            {
                StartMenuItem item = new StartMenuItem();

                item.Name = i.ApplicationName;
                item.Path = i.ApplicationExe;

                img = System.Drawing.Icon.ExtractAssociatedIcon(i.ApplicationExe);
                Bitmap bitmap = img.ToBitmap();
                IntPtr hBitmap = bitmap.GetHbitmap();
                ImageSource wpfBitmap = Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());

                item.Icon = wpfBitmap;


                StartMenuItems.Add(item);
            }*/

            Icon img = null;

            DirectoryInfo dir = new DirectoryInfo(@"C:\ProgramData\Microsoft\Windows\Start Menu\Programs");
            FileInfo[] files = dir.GetFiles("*");

            foreach(FileInfo file1 in files)
            {
                Console.WriteLine(file1.FullName);

                    if (System.IO.File.Exists(file1.FullName))
                     {
                    if (file1.FullName.EndsWith("lnk"))
                    {
                        IWshShell shell = new WshShell();
                        var lnk = shell.CreateShortcut(file1.FullName) as IWshShortcut;
                        if (lnk != null)
                        {



                            try
                            {
                                StartMenuItem item = new StartMenuItem();

                                item.Path = lnk.TargetPath;
                                item.Name = System.IO.Path.GetFileNameWithoutExtension(file1.FullName);

                                img = System.Drawing.Icon.ExtractAssociatedIcon(lnk.TargetPath);
                                Bitmap bitmap = img.ToBitmap();
                                IntPtr hBitmap = bitmap.GetHbitmap();
                                ImageSource wpfBitmap = Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());

                                item.Icon = wpfBitmap;


                                StartMenuItems.Add(item);
                            }
                            catch (Exception fileerr)
                            {

                            }


                        }
                         }

                     }
                }

            AppsList.ItemsSource = StartMenuItems;
        }

        public void Test(string path)
        {
            Process p = new Process();
            p.StartInfo.FileName = path;
            p.StartInfo.CreateNoWindow = false;
            p.Start();
        }

        public void HideApp()
        {
            this.Hide();
        }


        private async void ListView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


            if (e.AddedItems != null && e.AddedItems.Count > 0)
            {
                StartMenuItem selectedRow = e.AddedItems[0] as StartMenuItem;
                if (selectedRow != null)
                {
                    string process = selectedRow.Path;

                    Test(process);


                    HideApp();

                    AppsList.SelectedItems.Clear();
                }
            }
        }

        private void Label_MouseDown(object sender, MouseButtonEventArgs e)
        {
            HideApp();
            // Cairo.ShowLogOffConfirmation();
            Message m = new Message("", "Do you want to put your pc to sleep?", "Your work will be unaffected.", "sleep");
            m.Show();
        }

        private void Label_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            HideApp();
            //Cairo.ShowShutdownConfirmation();
            Message m = new Message("Do you want to shutdown?", "Do you want to shutdown your pc?", "Make sure your work is saved.", "shutdown");
            m.Show();
        }

        private void Label_MouseDown_2(object sender, MouseButtonEventArgs e)
        {
            HideApp();
            //Cairo.ShowRebootConfirmation();
            Message m = new Message("Do you want to reboot?", "Do you want to reboot your pc?", "Make sure your work is saved.", "reboot");
            m.Show();
        }

        bool ss = true;
        public void togglepower()
        {
            if(ss == false)
            {
                poweroptions.Visibility = Visibility.Hidden;
                ss = true;
            } else
            {
                poweroptions.Visibility = Visibility.Visible;
                ss = false;
            }
        }

        private void Label_MouseDown_3(object sender, MouseButtonEventArgs e)
        {
            togglepower();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Top = SystemParameters.PrimaryScreenHeight - (this.Height + 44);
            this.Left = 0;
        }

        private void Label_MouseDown_4(object sender, MouseButtonEventArgs e)
        {
            HideApp();
            Settings s = new Settings();
            s.Show();
        }
    }



    public class StartMenuItem
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public ImageSource Icon { get; set; }
    }
}
