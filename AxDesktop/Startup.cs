﻿namespace CairoDesktop
{
    using System;
    using System.Diagnostics;
    using System.Windows;
    using Microsoft.Win32;
    using Interop;
    using System.Collections.Generic;
    using System.Windows.Threading;
    using System.Windows.Markup;
    using System.Threading.Tasks;
    using SupportingClasses;
    using Common;
    using CairoDesktop.WindowsTray;
    using System.Threading;
    using CairoDesktop.Common.Logging;
    using CairoExplorer;
    using System.Windows.Input;
    using System.Runtime.InteropServices;
    using System.Drawing;
    using System.Reflection;
    using System.IO;
    using System.Linq;
    using AxDesktop;

    /// <summary>
    /// Handles the startup of the application, including ensuring that only a single instance is running.
    /// </summary>
    public partial class Startup
    {
        private static System.Threading.Mutex cairoMutex;

        public static Taskbar TaskbarWindow { get; set; }
        public static List<Taskbar> TaskbarWindows = new List<Taskbar>();

        public static Desktop DesktopWindow { get; set; }
        public static Desktop2 DesktopWindow2 { get; set; }

        public static bool IsCairoUserShell;

        private static CommandLineParser commandLineParser;
        private static bool isRestart;
        private static bool isTour;

        public static bool IsShuttingDown { get; set; }

        public static bool IsSettingScreens { get; set; }

        private static System.Windows.Forms.Screen[] screenState = { }; 
        private static Object screenSetupLock = new Object();

        /// <summary>
        /// The main entry point for the application
        /// </summary>
        [STAThread]
        public static void Main(string[] args)
        {
            #region Initialization Routines

            ProcessCommandLineArgs(args);
            SingleInstanceCheck();
            SetShellReadyEvent();

            //SetupSettings(); // run this before logging setup so that preferences are always used
            SetupLoggingSystem();
            WriteApplicationDebugInfoToConsole();

            SetupPluginSystem();
            
            #endregion

            // check if we are the current user's shell
            // set here as well so that we don't behave differently once user changes setting
            IsCairoUserShell = Shell.IsCairoUserShell;

            Shell.ToggleDesktopIcons(true);
            
            App app = new App();
            app.InitializeComponent();

            // Set custom theme if selected


            RegistryKey regKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\WinLogon", true);
            regKey.SetValue("Shell", AppDomain.CurrentDomain.BaseDirectory + AppDomain.CurrentDomain.FriendlyName);

            //RegistryKey regKey1 = Registry.CurrentUser.OpenSubKey("Control Panel\\Cursors", true);
            //regKey.SetValue("Hand", @"C:\AxOS\Resources\Mouse\Hand.cur");


            AppBarHelper.SetWinTaskbarState(AppBarHelper.WinTaskbarState.AutoHide);
            AppBarHelper.SetWinTaskbarPos((int)NativeMethods.SetWindowPosFlags.SWP_HIDEWINDOW);

            DesktopWindow = new Desktop();
            DesktopWindow.Show();

            try
            {
                DesktopWindow2 = new Desktop2();
                DesktopWindow2.Show();
            } catch(Exception e)
            {

            }

            TaskbarWindow = new Taskbar(System.Windows.Forms.Screen.PrimaryScreen);
            TaskbarWindow.Show();
            TaskbarWindows.Add(TaskbarWindow);

            if(!File.Exists("C:/AxOS/setup.ax"))
            {
                Setup s = new Setup();
                s.Show();
            }

            ScreenSetup(true);
            

            // initialize system tray if enabled
            NotificationArea.Instance.Initialize();
            
            // login items only necessary if Explorer didn't start them
            if (IsCairoUserShell && !isRestart)
            {
                //RunStartupApps();
            }

            ChangeCursor(@"C:\AxOS\Resources\Mouse\Normal.cur");
            ChangeHand(@"C:\AxOS\Resources\Mouse\Hand.cur");
            ChangeStart(@"C:\AxOS\Resources\Mouse\Working.ani");
            ChangeWait(@"C:\AxOS\Resources\Mouse\Busy.ani");


            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += clock_Tick;
            timer.Start();
            test();
            app.Run();
        }

        public static void test()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
            {

                Assembly thisAssembly = Assembly.GetExecutingAssembly();

                //Get the Name of the AssemblyFile
                var name = args.Name.Substring(0, args.Name.IndexOf(',')) + ".dll";

                //Load form Embedded Resources - This Function is not called if the Assembly is in the Application Folder
                var resources = thisAssembly.GetManifestResourceNames().Where(s => s.EndsWith(name));
                if (resources.Count() > 0)
                {
                    var resourceName = resources.First();
                    using (Stream stream = thisAssembly.GetManifestResourceStream(resourceName))
                    {
                        if (stream == null) return null;
                        var block = new byte[stream.Length];
                        stream.Read(block, 0, block.Length);
                        return Assembly.Load(block);
                    }
                }
                return null;
            };
        }

        //Adaptive Cursor

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr GetDesktopWindow();
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr GetWindowDC(IntPtr window);
        [DllImport("gdi32.dll", SetLastError = true)]
        public static extern uint GetPixel(IntPtr dc, int x, int y);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern int ReleaseDC(IntPtr window, IntPtr dc);
    

        private static void clock_Tick(object sender, EventArgs e)
        {
           // getAdaptiveCursor();
        }

        [DllImport("user32.dll")]
        static extern bool GetCursorPos(out POINT lpPoint);

        public struct POINT
        {
            public int X;
            public int Y;
        }

        public static void getAdaptiveCursor()
        {

            POINT point;
            GetCursorPos(out point);

            if (GetColorAt(point.X, point.Y).R > 200 && GetColorAt(point.X, point.Y).G > 200 && GetColorAt(point.X, point.Y).B > 200)
            {
                ChangeCursor(@"C:\AxOS\Resources\Mouse\Adaptive\Normal.cur");
                ChangeHand(@"C:\AxOS\Resources\Mouse\Adaptive\Hand.cur");
                //Console.WriteLine(point.X.ToString() + "," + point.Y.ToString());
            } else
            {
                ChangeCursor(@"C:\AxOS\Resources\Mouse\Normal.cur");
                ChangeHand(@"C:\AxOS\Resources\Mouse\Hand.cur");
                //Console.WriteLine(point.X.ToString() + "," + point.Y.ToString() + " = " + GetColorAt(point.X, point.Y));
            }
        }


        public static Color GetColorAt(int x, int y)
        {
            IntPtr desk = GetDesktopWindow();
            IntPtr dc = GetWindowDC(desk);
            int a = (int)GetPixel(dc, x, y);
            ReleaseDC(desk, dc);
            return Color.FromArgb(255, (a >> 0) & 0xff, (a >> 8) & 0xff, (a >> 16) & 0xff);
        }





        private static void ChangeCursor(string curFile)
        {
            Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\Cursors\", "Arrow", curFile);
            SystemParametersInfo(SPI_SETCURSORS, 0, 0, SPIF_UPDATEINIFILE | SPIF_SENDCHANGE);
        }

        private static void ChangeHand(string curFile)
        {
            Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\Cursors\", "Hand", curFile);
            SystemParametersInfo(SPI_SETCURSORS, 0, 0, SPIF_UPDATEINIFILE | SPIF_SENDCHANGE);
        }

        private static void ChangeStart(string curFile)
        {
            Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\Cursors\", "AppStarting", curFile);
            SystemParametersInfo(SPI_SETCURSORS, 0, 0, SPIF_UPDATEINIFILE | SPIF_SENDCHANGE);
        }

        private static void ChangeWait(string curFile)
        {
            Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\Cursors\", "Wait", curFile);
            SystemParametersInfo(SPI_SETCURSORS, 0, 0, SPIF_UPDATEINIFILE | SPIF_SENDCHANGE);
        }


        const int SPI_SETCURSORS = 0x0057;
        const int SPIF_UPDATEINIFILE = 0x01;
        const int SPIF_SENDCHANGE = 0x02;
        private readonly Dispatcher Dispatcher;

        [DllImport("user32.dll", EntryPoint = "SystemParametersInfo")]
        public static extern bool SystemParametersInfo(uint uiAction, uint uiParam, uint pvParam, uint fWinIni);

        public static void ResetScreenCache()
        {
            // use reflection to empty screens cache
            typeof(System.Windows.Forms.Screen).GetField("screens", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic).SetValue(null, null);
        }

        /// <summary>
        /// Compares the system screen list to the screens associated with Cairo windows, then creates or destroys windows as necessary.
        /// Only affects non-primary screens, as Cairo always opens on at least the primary screen.
        /// Runs at startup and when a WM_DISPLAYCHANGE message is received by the main MenuBar window.
        /// </summary>
        public static void ScreenSetup(bool skipChecks = false)
        {

            lock (screenSetupLock)
            {
                CairoLogger.Instance.Debug("Beginning screen setup");
                IsSettingScreens = true;

                bool shouldSetScreens = true;

                List<string> sysScreens = new List<string>();
                List<string> openScreens = new List<string>();
                List<string> addedScreens = new List<string>();
                List<string> removedScreens = new List<string>();

                ResetScreenCache();

                if (screenState.Length == System.Windows.Forms.Screen.AllScreens.Length)
                {
                    bool same = true;
                    for (int i = 0; i < screenState.Length; i++)
                    {
                        System.Windows.Forms.Screen current = System.Windows.Forms.Screen.AllScreens[i];
                        if (!(screenState[i].Bounds == current.Bounds && screenState[i].DeviceName == current.DeviceName && screenState[i].Primary == current.Primary && screenState[i].WorkingArea == current.WorkingArea))
                        {
                            same = false;
                            break;
                        }
                    }

                    if (same)
                    {
                        CairoLogger.Instance.Debug("Skipping screen setup due to no differences");
                        shouldSetScreens = false;
                    }
                    else
                        screenState = System.Windows.Forms.Screen.AllScreens;
                }
                else
                    screenState = System.Windows.Forms.Screen.AllScreens;

                if (shouldSetScreens)
                {
                    if (!skipChecks)
                    {
                            // enumerate screens

                            foreach (Taskbar bar in TaskbarWindows)
                            {
                                if (bar.Screen != null)
                                    openScreens.Add(bar.Screen.DeviceName);
                            }

                        foreach (var screen in screenState)
                        {
                            CairoLogger.Instance.Debug(string.Format("{0} found at {1} with area {2}; primary? {3}", screen.DeviceName, screen.Bounds.ToString(), screen.WorkingArea.ToString(), screen.Primary.ToString()));

                            sysScreens.Add(screen.DeviceName);
                        }

                        // figure out which screens have been added vs removed

                        foreach (string name in sysScreens)
                        {
                            if (!openScreens.Contains(name))
                                addedScreens.Add(name);
                        }

                        foreach (string name in openScreens)
                        {
                            if (!sysScreens.Contains(name))
                                removedScreens.Add(name);
                        }

                        if (removedScreens.Count == sysScreens.Count)
                        {
                            // remove everything?! no way!
                            return;
                        }

                        // close windows associated with removed screens
                        foreach (string name in removedScreens)
                        {
                            CairoLogger.Instance.Debug("Removing windows associated with screen " + name);

                            // close taskbars
                            Taskbar taskbarToClose = null;
                            foreach (Taskbar bar in TaskbarWindows)
                            {
                                if (bar.Screen != null && bar.Screen.DeviceName == name)
                                {
                                    taskbarToClose = bar;
                                    break;
                                }
                            }

                            if (taskbarToClose != null)
                            {
                                taskbarToClose.Close();
                                TaskbarWindows.Remove(taskbarToClose);
                            }

                            // close menu bars

                            // close menu bar shadows

                        }

                        CairoLogger.Instance.Debug("Refreshing screen information for stale windows");

                        // update screens of stale windows

                        foreach (Taskbar bar in TaskbarWindows)
                        {
                            if (bar.Screen != null)
                            {
                                foreach (System.Windows.Forms.Screen screen in screenState)
                                {
                                    if (screen.DeviceName == bar.Screen.DeviceName)
                                    {
                                        bar.Screen = screen;
                                        bar.setPosition();
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    // open windows on newly added screens
                    foreach (var screen in screenState)
                    {
                        if ((skipChecks && !screen.Primary) || addedScreens.Contains(screen.DeviceName))
                        {
                            CairoLogger.Instance.Debug("Opening windows on screen " + screen.DeviceName);

                            /*if (Settings.EnableMenuBarMultiMon)
                            {
                                if (screen.Primary)
                                    CairoLogger.Instance.Debug("Opening menu bar on new primary display");

                                // menu bars
                                MenuBar newMenuBar = new MenuBar(screen);
                                newMenuBar.Show();
                                MenuBarWindows.Add(newMenuBar);

                                if (Settings.EnableMenuBarShadow)
                                {
                                    // menu bar shadows
                                    MenuBarShadow newMenuBarShadow = new MenuBarShadow(newMenuBar, screen);
                                    newMenuBarShadow.Show();
                                    MenuBarShadowWindows.Add(newMenuBarShadow);
                                }
                            } */

                                // taskbars
                                Taskbar newTaskbar = new Taskbar(screen);
                                newTaskbar.Show();
                                TaskbarWindows.Add(newTaskbar);
                        }

                        // Set desktop work area for when Explorer isn't running
                            AppBarHelper.SetWorkArea(screen);
                    }
                }

                IsSettingScreens = false;
                CairoLogger.Instance.Debug("Completed screen setup");
            }
        }

        /// <summary>
        /// Executes the first run sequence.
        /// </summary>


        public static void Restart()
        {
            try
            {
                // run the program again
                Process current = new Process();
                current.StartInfo.FileName = AppDomain.CurrentDomain.BaseDirectory + "CairoDesktop.exe";
                current.StartInfo.Arguments = "/restart";
                current.Start();

                // close this instance
                Shutdown();
            }
            catch
            { }
        }

        public static void Shutdown()
        {
            if (IsCairoUserShell)
            {
                Shell.StartProcess("explorer.exe");
            }

            IsShuttingDown = true;

            Application.Current?.Dispatcher.Invoke(() => Application.Current?.Shutdown(), DispatcherPriority.Normal);
        }

        #region Autorun

        private static List<string> FetchStartupApps()
        {
            List<string> startupApps = new List<string>();

            // Registry startup keys
            Dictionary<string, string> startupKeys = new Dictionary<string, string>()
            {
                { "Software\\Microsoft\\Windows\\CurrentVersion\\Run", "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\StartupApproved\\Run" },
                { "Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Run", "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\StartupApproved\\Run32" },
                { "Software\\Microsoft\\Windows\\CurrentVersion\\RunOnce", "" }
            };

            // loop twice, once for HKLM once for HKCU
            for (int i = 0; i <= 1; i++)
            {
                foreach (KeyValuePair<string, string> regKey in startupKeys)
                {
                    bool isRunOnce = regKey.Key.Contains("RunOnce");

                    // key is the reg key with the app path, value is the startupapproved key telling us if the item is disabled
                    RegistryKey root = null;
                    RegistryKey key = null;
                    RegistryKey approvedKey = null;

                    try
                    {
                        if (i == 0)
                            root = Registry.LocalMachine;
                        else
                            root = Registry.CurrentUser;

                        if (isRunOnce && i != 0)
                            key = root.OpenSubKey(regKey.Key, true);
                        else if (isRunOnce)
                            continue; // skip processing HKLM RunOnce because we can't remove things from there
                        else
                        {
                            key = root.OpenSubKey(regKey.Key, false);
                            approvedKey = root.OpenSubKey(regKey.Value, false);
                        }
                    }
                    catch { continue; } // in case of unable to load registry key
                    
                    if (key != null && key.ValueCount > 0)
                    {
                        foreach (string valueName in key.GetValueNames())
                        {
                            bool canRun = true;

                            if (approvedKey != null)
                            {
                                foreach (string approvedName in approvedKey.GetValueNames())
                                {
                                    try
                                    {
                                        string s = ((byte[])approvedKey.GetValue(approvedName))[0].ToString();
                                        if (approvedName == valueName && ((byte[])approvedKey.GetValue(approvedName))[0] % 2 != 0) // if value is odd number, item is disabled
                                        {
                                            canRun = false;
                                            break;
                                        }
                                        else if (approvedName == valueName)
                                            break;
                                    }
                                    catch { } // in case of invalid registry key values
                                }
                            }

                            if (canRun)
                                startupApps.Add(((string)key.GetValue(valueName)).Replace("\"", ""));

                            // if this is a runonce key, remove the value after we grab it
                            if (isRunOnce)
                            {
                                try
                                {
                                    key.DeleteValue(valueName);
                                }
                                catch { }
                            }
                        }
                    }

                    if (key != null)
                        key.Close();

                    if (approvedKey != null)
                        approvedKey.Close();
                }
            }

            // startup folders
            Dictionary<SystemDirectory, RegistryKey> startupFolderKeys = new Dictionary<SystemDirectory, RegistryKey>()
            {
                { new SystemDirectory(Environment.GetFolderPath(Environment.SpecialFolder.CommonStartup), Dispatcher.CurrentDispatcher), Registry.LocalMachine.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\StartupApproved\\StartupFolder", false) },
                { new SystemDirectory(Environment.GetFolderPath(Environment.SpecialFolder.Startup), Dispatcher.CurrentDispatcher), Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\StartupApproved\\StartupFolder", false) }
            };
            
            foreach (KeyValuePair<SystemDirectory, RegistryKey> startupFolder in startupFolderKeys)
            {
                foreach (SystemFile startupFile in startupFolder.Key.Files)
                {
                    bool canRun = true;

                    if (startupFolder.Value != null)
                    {
                        foreach (string approvedName in startupFolder.Value.GetValueNames())
                        {
                            try
                            {
                                string s = ((byte[])startupFolder.Value.GetValue(approvedName))[0].ToString();
                                if (approvedName == startupFile.Name && ((byte[])startupFolder.Value.GetValue(approvedName))[0] % 2 != 0) // if value is odd number, item is disabled
                                {
                                    canRun = false;
                                    break;
                                }
                                else if (approvedName == startupFile.Name)
                                    break;
                            }
                            catch { } // in case of invalid registry key values
                        }
                    }

                    if (canRun)
                        startupApps.Add(startupFile.FullName);
                }
            }

            return startupApps;
        }

        private static string[] expandArgs(string startupPath)
        {
            string[] procInfo = new string[2];

            int exeIndex = startupPath.IndexOf(".exe");

            if(exeIndex > 0)
            {
                // we may have args for an executable
                if (exeIndex + 4 != startupPath.Length)
                {
                    // argh, args!
                    procInfo[0] = startupPath.Substring(0, exeIndex + 4);
                    procInfo[1] = startupPath.Substring(exeIndex + 5, startupPath.Length - exeIndex - 5);
                }
                else
                {
                    procInfo[0] = startupPath;
                }
            }
            else
            {
                // no args to parse out
                procInfo[0] = startupPath;
            }

            return procInfo;
        }

        private async static void RunStartupApps()
        {
            await Task.Run(() => LoopStartupApps());
        }

        private static void LoopStartupApps()
        {
            foreach (string app in FetchStartupApps())
            {
                string[] procInfo = expandArgs(app);

                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.UseShellExecute = true;
                startInfo.FileName = procInfo[0];
                startInfo.Arguments = procInfo[1];

                CairoLogger.Instance.Debug("Starting program: " + startInfo.FileName);

                try
                {
                    Process.Start(startInfo);
                }
                catch { }
            }
        }

        #endregion
    }
}
