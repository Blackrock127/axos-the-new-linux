﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Windows.Interop;
using CairoDesktop.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.VisualBasic.FileIO;
using Microsoft.Win32;
using System.IO;
using CairoDesktop.SupportingClasses;
using CairoDesktop.Common;
using System.Windows.Threading;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Forms;

namespace CairoDesktop
{
    /// <summary>
    /// Interaction logic for Desktop.xaml
    /// </summary>
    public partial class Desktop1 : Window, INotifyPropertyChanged
    {
        private WindowInteropHelper helper;
        private bool altF4Pressed;
        public System.Windows.Forms.Screen Screen;
        private double dpiScale = 1.0;
        public Stack<string> PathHistory = new Stack<string>();
        public DesktopIcons Icons;
        System.Drawing.Rectangle r = Screen.AllScreens[1].WorkingArea;
        //public DependencyProperty IsOverlayOpenProperty = DependencyProperty.Register("IsOverlayOpen", typeof(bool), typeof(Desktop), new PropertyMetadata(new bool()));

        public Desktop1()
        {
            Left = r.Left;
            Top = r.Top;

            Height = r.Height - 1;
            Width = r.Width; 

            InitializeComponent();
            //Location = System.Windows.Forms.Screen.AllScreens[1].WorkingArea.Location;

            if (Startup.IsCairoUserShell)
            {
            }

            setGridPosition();
            //setBackground();
            initializeClock();
        }

        public void ResetPosition()
        {
            setGridPosition();
        }

        private void initializeClock()
        {
            // initial display
            clock_Tick();

            // Create our timer for clock
            DispatcherTimer clock = new DispatcherTimer(new TimeSpan(0, 0, 0, 0, 500), DispatcherPriority.Background, delegate
            {
                clock_Tick();
            }, this.Dispatcher);
        }

        private void clock_Tick()
        {
            string test1 = "";

            using (StreamReader readtext = new StreamReader("C:/AxOS/Wallpaper.ax"))
            {
                string readMeText = readtext.ReadLine();
                test1 = readMeText;
            }

            ImageSource myimage = new BitmapImage(new Uri(test1));
            test.ImageSource = myimage;
        }



        private void setBackground()
        {
            if (Startup.IsCairoUserShell)
            {
                // draw wallpaper
                string regWallpaper = Registry.GetValue(@"HKEY_CURRENT_USER\Control Panel\Desktop", "Wallpaper", "") as string;
                if (!string.IsNullOrWhiteSpace(regWallpaper) && Shell.Exists(regWallpaper))
                    TryAndEat(() => Background = new ImageBrush { ImageSource = new BitmapImage(new Uri(regWallpaper, UriKind.Absolute)) });
            }
        }

        private void SetupPostInit()
        {
            Shell.HideWindowFromTasks(helper.Handle);
        }

        public IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (msg == NativeMethods.WM_MOUSEACTIVATE)
            {
                handled = true;
                return new IntPtr(NativeMethods.MA_NOACTIVATE);
            }
            else if (msg == NativeMethods.WM_WINDOWPOSCHANGING)
            {
                /*// Extract the WINDOWPOS structure corresponding to this message
                NativeMethods.WINDOWPOS wndPos = NativeMethods.WINDOWPOS.FromMessage(lParam);

                // Determine if the z-order is changing (absence of SWP_NOZORDER flag)
                if (!((wndPos.flags & NativeMethods.SetWindowPosFlags.SWP_NOZORDER) == NativeMethods.SetWindowPosFlags.SWP_NOZORDER))
                {
                    // add the SWP_NOZORDER flag
                    wndPos.flags = wndPos.flags | NativeMethods.SetWindowPosFlags.SWP_NOZORDER;
                    wndPos.UpdateMessage(lParam);
                }*/

                handled = true;
                return new IntPtr(NativeMethods.MA_NOACTIVATE);
            }
            else if (msg == NativeMethods.WM_DISPLAYCHANGE && (Startup.IsCairoUserShell))
            {
               // SetPosition(((uint)lParam & 0xffff), ((uint)lParam >> 16));
                handled = true;
            }

            return IntPtr.Zero;
        }

        private void SetPosition(uint x, uint y)
        {
            Top = 0;
            Left = 0;

            Width = x;
            Height = y - 1;
            setGridPosition();
        }

        private void setGridPosition()
        {
        }


        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (Startup.IsShuttingDown) // show the windows desktop
                Shell.ToggleDesktopIcons(true);
            else if (altF4Pressed) // Show the Shutdown Confirmation Window
            {
                e.Cancel = true;
            }
            else // Eat it !!!
                e.Cancel = true;
        }


        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Alt && e.SystemKey == Key.F4)
            {
                altF4Pressed = true;
            }
        }

        private void PasteFromClipboard()
        {
            System.Windows.IDataObject clipFiles = System.Windows.Clipboard.GetDataObject();
            if (clipFiles.GetDataPresent(System.Windows.DataFormats.FileDrop))
                if (clipFiles.GetData(System.Windows.DataFormats.FileDrop) is string[] files)
                    foreach (string file in files)
                        if (Shell.Exists(file))
                            TryAndEat(() =>
                            {
                                //FileAttributes attr = File.GetAttributes(file);
                                //if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                                    //FileSystem.CopyDirectory(file, Icons.Location.FullName + "\\" + new DirectoryInfo(file).Name, UIOption.AllDialogs);
                                //else
                                    //FileSystem.CopyFile(file, Icons.Location.FullName + "\\" + Path.GetFileName(file), UIOption.AllDialogs);
                            });

        }


        private void miPaste_Click(object sender, RoutedEventArgs e)
        {
            PasteFromClipboard();
        }

        private void miPersonalization_Click(object sender, RoutedEventArgs e)
        {
            // doesn't work when shell because Settings app requires Explorer :(
            if (!Shell.StartProcess("desk.cpl"))
                CairoMessage.Show("Unable to open Personalization settings.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void grid_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!Topmost)
                NativeMethods.SetForegroundWindow(helper.Handle);
        }

        public void Navigate(string newLocation)
        {
            //PathHistory.Push(Icons.Location.FullName);
            //Icons.Location.Dispose();
            //Icons.Location = new SystemDirectory(newLocation, Dispatcher.CurrentDispatcher);
            OnPropertyChanged("CurrentDirectoryFriendly");
        }

        public string CurrentLocation
        {
            get
            {
                return null;
            }
            set
            {
                //Icons.Location = new SystemDirectory(value, Dispatcher.CurrentDispatcher);
                OnPropertyChanged("CurrentDirectoryFriendly");
            }
        }

        private void ShowOverlay()
        {
            Topmost = true;
            NativeMethods.SetForegroundWindow(helper.Handle);
            Background = null;
        }

        private void CloseOverlay()
        {
            Topmost = false;
            Shell.ShowWindowBottomMost(helper.Handle);
            setBackground();
        }

        public string CurrentDirectoryFriendly
        {
            get
            {
                //return Localization.DisplayString.sDesktop_CurrentFolder + " " + Icons.Location.FullName;
                return null;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void TryAndEat(Action action)
        {
            try { action.Invoke(); }
            catch { }
        }

      
    }
}