﻿using System;
using System.Windows;
using CairoDesktop.SupportingClasses;
using System.Windows.Interop;
using CairoDesktop.Interop;
using System.Windows.Threading;
using System.Collections.Specialized;
using CairoDesktop.WindowsTray;
using System.Windows.Controls.Primitives;
using CairoExplorer;
using CairoDesktop.Common;

namespace CairoDesktop
{
    /// <summary>
    /// Interaction logic for Taskbar.xaml
    /// </summary>
    public partial class Taskbar : Window
    {
        public System.Windows.Forms.Screen Screen;
        private double dpiScale = 1.0;
        private static bool isProgramsMenuHotkeyRegistered = false;

        public bool IsClosing = false;

        // AppBar properties
        private WindowInteropHelper helper;
        public IntPtr handle;
        private int appbarMessageId = -1;
        private bool displayChanged = false;
        private AppBarHelper.ABEdge appBarEdge = AppBarHelper.ABEdge.ABE_BOTTOM;
        private int addToSize = 0;

        public AppGrabber.AppGrabber appGrabber = AppGrabber.AppGrabber.Instance;

        public static DependencyProperty ButtonWidthProperty = DependencyProperty.Register("ButtonWidth", typeof(double), typeof(Taskbar), new PropertyMetadata(new double()));
        public double ButtonWidth
        {
            get { return (double)GetValue(ButtonWidthProperty); }
            set { SetValue(ButtonWidthProperty, value); }
        }

        public static DependencyProperty ButtonTextWidthProperty = DependencyProperty.Register("ButtonTextWidth", typeof(double), typeof(Taskbar), new PropertyMetadata(new double()));
        public double ButtonTextWidth
        {
            get { return (double)GetValue(ButtonTextWidthProperty); }
            set { SetValue(ButtonTextWidthProperty, value); }
        }
        
        public Taskbar() : this(System.Windows.Forms.Screen.PrimaryScreen)
        {
            
        }

        public Taskbar(System.Windows.Forms.Screen screen)
        {
            Screen = screen;

            InitializeComponent();
            setupPostInit();
            setupTaskbar();
        }

        private void setupTaskbar()
        {
            double screenWidth = screenWidth = Screen.Bounds.Width / dpiScale;
            Left = Screen.Bounds.Left / dpiScale;

            this.DataContext = WindowsTasks.WindowsTasksService.Instance;
            grdTaskbar.DataContext = WindowsTasks.WindowsTasksService.Instance;
            AppGrabber.Category quickLaunch = appGrabber.QuickLaunch;
            
            this.quickLaunchList.ItemsSource = quickLaunch;
            this.bdrTaskbar.MaxWidth = screenWidth - 36;
            this.Width = screenWidth;

            addToSize = 16;

            this.Height = 29 + addToSize;

            ((INotifyCollectionChanged)TasksList.Items).CollectionChanged += TasksList_Changed;

            /*if (Startup.DesktopWindow != null)
                btnDesktopOverlay.DataContext = Startup.DesktopWindow;
            else
                btnDesktopOverlay.Visibility = Visibility.Collapsed;*/

                // set taskbar edge based on preference
                /*appBarEdge = AppBarHelper.ABEdge.ABE_TOP;
                bdrTaskbar.Style = Application.Current.FindResource("CairoTaskbarTopBorderStyle") as Style;
                bdrTaskbarEnd.Style = Application.Current.FindResource("CairoTaskbarEndTopBorderStyle") as Style;
                bdrTaskbarLeft.Style = Application.Current.FindResource("CairoTaskbarLeftTopBorderStyle") as Style;
                btnTaskList.Style = Application.Current.FindResource("CairoTaskbarTopButtonList") as Style;
                btnDesktopOverlay.Style = Application.Current.FindResource("CairoTaskbarTopButtonDesktopOverlay") as Style;
                TaskbarGroupStyle.ContainerStyle = Application.Current.FindResource("CairoTaskbarTopGroupStyle") as Style;
                TasksList.Margin = new Thickness(0);
                bdrTaskListPopup.Margin = new Thickness(5, this.Top + this.Height - 1, 5, 11);*/

            // show task view on windows >= 10, adjust margin if not shown
            /*if (Shell.IsWindows10OrBetter && !Startup.IsCairoUserShell)
                bdrTaskView.Visibility = Visibility.Visible;
            else
                TasksList2.Margin = new Thickness(0, -3, 0, -3);*/
        }

        private void Taskbar_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            IsClosing = true;
            if (Startup.IsShuttingDown && Screen.Primary)
            {
                // Manually call dispose on window close...
                (this.DataContext as WindowsTasks.WindowsTasksService).Dispose();

                // dispose system tray if it's still running to prevent conflicts when doing AppBar stuff
                NotificationArea.Instance.Dispose();

                if (AppBarHelper.appBars.Contains(this.handle))
                    AppBarHelper.RegisterBar(this, Screen, this.ActualWidth * dpiScale, this.ActualHeight * dpiScale);

                // show the windows taskbar again
                AppBarHelper.SetWinTaskbarState(AppBarHelper.WinTaskbarState.OnTop);
                AppBarHelper.SetWinTaskbarPos((int)NativeMethods.SetWindowPosFlags.SWP_SHOWWINDOW);
            }
            else if (Startup.IsSettingScreens || Startup.IsShuttingDown)
            {
                if (AppBarHelper.appBars.Contains(this.handle))
                    AppBarHelper.RegisterBar(this, Screen, this.ActualWidth, this.ActualHeight);
            }
            else
            {
                IsClosing = false;
                e.Cancel = true;
            }
        }

        private void setTaskButtonSize()
        {
            ButtonWidth = 28 + addToSize;
        }

        public void setPosition()
        {
            double screenWidth = Screen.Bounds.Width / dpiScale;
            double screenHeight = Screen.Bounds.Height / dpiScale;

            setTopPosition(Screen.Bounds.Bottom / dpiScale);
            
            this.Left = Screen.Bounds.Left / dpiScale;

            this.bdrTaskbar.MaxWidth = screenWidth - 36;
            this.Width = screenWidth;
        }

        private void setPosition(uint x, uint y)
        {
            displayChanged = true;
            int sWidth;
            int sHeight;
            // adjust size for dpi
            Shell.TransformFromPixels(x, y, out sWidth, out sHeight);
            
            setTopPosition(Screen.Bounds.Bottom / dpiScale);

            this.Left = Screen.Bounds.Left / dpiScale;

            this.bdrTaskbar.MaxWidth = sWidth - 36;
            this.Width = sWidth;
        }

        private void setTopPosition(double top)
        {

            this.Top = top - this.Height;

            /*if (Settings.TaskbarPosition == 1)
            {
                // set to bottom of menu bar
                this.Top = (Screen.Bounds.Y / dpiScale) + Startup.MenuBarWindow.Height;
            }
            else
            {
                // set to bottom of workspace
                this.Top = top - this.Height;
            }*/
        }

        public void SetFullScreenMode(bool entering)
        {
            if (entering)
            {
                this.Topmost = false;
                Shell.ShowWindowBottomMost(handle);
            }
            else
            {
                takeFocus(); // unable to set topmost unless we do this

                this.Topmost = true;
                Shell.ShowWindowTopMost(handle);
            }
        }


        private void OnShowProgramsMenu(HotKey hotKey)
        {
            ToggleProgramsMenu();
        }

        public IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (msg == NativeMethods.WM_MOUSEACTIVATE)
            {
                handled = true;
                return new IntPtr(NativeMethods.MA_NOACTIVATE);
            }

            /* if (msg == appbarMessageId && appbarMessageId != -1 && Settings.TaskbarMode == 0)
             {
                 switch ((NativeMethods.AppBarNotifications)wParam.ToInt32())
                 {
                     case NativeMethods.AppBarNotifications.PosChanged:
                         // Reposition to the top of the screen.
                         AppBarHelper.ABSetPos(this, Screen, this.ActualWidth * dpiScale, this.ActualHeight * dpiScale, appBarEdge);
                         break;

                     case NativeMethods.AppBarNotifications.FullScreenApp:
                         SetFullScreenMode((int)lParam == 1);

                         break;

                     case NativeMethods.AppBarNotifications.WindowArrange:
                         if ((int)lParam != 0)    // before
                             this.Visibility = Visibility.Collapsed;
                         else                         // after
                             this.Visibility = Visibility.Visible;

                         break;
                 }
                 handled = true;
             }
             else if (msg == NativeMethods.WM_ACTIVATE && Settings.TaskbarMode == 0)
             {
                 AppBarHelper.AppBarActivate(hwnd);
             }
             else if (msg == NativeMethods.WM_WINDOWPOSCHANGED && Settings.TaskbarMode == 0)
             {
                 AppBarHelper.AppBarWindowPosChanged(hwnd);
             }
             else if (msg == NativeMethods.WM_DPICHANGED)
             {
                 if (!(Settings.EnableMenuBarMultiMon || Settings.EnableTaskbarMultiMon))
                 {
                     Startup.ResetScreenCache();
                     Screen = System.Windows.Forms.Screen.PrimaryScreen;
                 }

                 if (Screen.Primary)
                     Shell.DpiScale = (wParam.ToInt32() & 0xFFFF) / 96d;
                 this.dpiScale = (wParam.ToInt32() & 0xFFFF) / 96d;
                 AppBarHelper.ABSetPos(this, Screen, this.ActualWidth * dpiScale, this.ActualHeight * dpiScale, appBarEdge);
             }
             else if (msg == NativeMethods.WM_DISPLAYCHANGE)
             {
                 if (!(Settings.EnableMenuBarMultiMon || Settings.EnableTaskbarMultiMon))
                 {
                     Startup.ResetScreenCache();
                     Screen = System.Windows.Forms.Screen.PrimaryScreen;
                 }

                 setPosition(((uint)lParam & 0xffff), ((uint)lParam >> 16));
                 handled = true;
             }

             return IntPtr.Zero;
         }*/
            return IntPtr.Zero;
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
            //Set the window style to noactivate.
            NativeMethods.SetWindowLong(helper.Handle, NativeMethods.GWL_EXSTYLE,
                NativeMethods.GetWindowLong(helper.Handle, NativeMethods.GWL_EXSTYLE) | NativeMethods.WS_EX_NOACTIVATE);
        }

        private void TaskbarWindow_SourceInitialized(object sender, EventArgs e)
        {
            helper = new WindowInteropHelper(this);

            HwndSource source = HwndSource.FromHwnd(helper.Handle);
            source.AddHook(new HwndSourceHook(WndProc));

            handle = helper.Handle;

            this.dpiScale = PresentationSource.FromVisual(this).CompositionTarget.TransformToDevice.M11;

            setPosition();
            setTaskButtonSize();
            initializeClock();
            /* if (Settings.TaskbarMode == 0)
                appbarMessageId = AppBarHelper.RegisterBar(this, Screen, this.ActualWidth * dpiScale, this.ActualHeight * dpiScale, appBarEdge);*/



            Shell.HideWindowFromTasks(handle);
        }

        private void setupPostInit()
        {
            // set initial DPI. We do it here so that we get the correct value when DPI has changed since initial user logon to the system.

            setPosition();
           // this.dpiScale = PresentationSource.FromVisual(this).CompositionTarget.TransformToDevice.M11;
            appbarMessageId = AppBarHelper.RegisterBar(this, Screen, this.ActualWidth * dpiScale, this.ActualHeight * dpiScale, AppBarHelper.ABEdge.ABE_TOP);

            Shell.HideWindowFromTasks(handle);

            if(Screen.Primary)
            {
                new HotKey(System.Windows.Input.Key.LWin, KeyModifier.Win | KeyModifier.NoRepeat, OnShowProgramsMenu);

                // HotKeyManager.RegisterHotKey(new List<string> { "Win", "RWin" }, OnShowProgramsMenu);
                new HotKey(System.Windows.Input.Key.RWin, KeyModifier.Win | KeyModifier.NoRepeat, OnShowProgramsMenu);

                isProgramsMenuHotkeyRegistered = true;
            }

            // Register Windows key to open Programs menu
            /*
             * This was modified to fix issue: Cairo incorrectly handles the [Win] key #193 
             */

            // HotKeyManager.RegisterHotKey(new List<string> { "Win", "LWin" }, OnShowProgramsMenu);

            /*if (Screen.Primary && keyboardListener == null)
            {
                keyboardListener = new LowLevelKeyboardListener();
                keyboardListener.OnKeyPressed += keyboardListener_OnKeyPressed;
                keyboardListener.HookKeyboard();
            }*/

        }

        private void initializeClock()
        {
            // initial display
            clock_Tick();

            // Create our timer for clock
            DispatcherTimer clock = new DispatcherTimer(new TimeSpan(0, 0, 0, 0, 500), DispatcherPriority.Background, delegate
            {
                clock_Tick();
            }, this.Dispatcher);
        }

        private void clock_Tick()
        {
            dateText.Text = string.Format("{0:hh:mm tt}", DateTime.Now);
        }

        private void TasksList_Changed(object sender, NotifyCollectionChangedEventArgs e)
        {
            setTaskButtonSize();
        }

        private void CollectionViewSource_Filter(object sender, System.Windows.Data.FilterEventArgs e)
        {
            WindowsTasks.ApplicationWindow window = e.Item as WindowsTasks.ApplicationWindow;

            if (window.ShowInTaskbar)
                e.Accepted = true;
            else
                e.Accepted = false;
        }

        private void TaskbarWindow_LocationChanged(object sender, EventArgs e)
        {
            // this variable is set when the display size is changed, since that event handles this function. if we run here too, wrong position is set
            if (!displayChanged)
                setPosition();
            else
            {
                displayChanged = false;

                    // set position after 2 seconds anyway in case we missed something
                    var timer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(2) };
                    timer.Start();
                    timer.Tick += (sender1, args) =>
                    {
                        setPosition();
                        timer.Stop();
                    };
            }
        }

        private void TaskView_Click(object sender, RoutedEventArgs e)
        {
            Shell.ShowWindowSwitcher();
        }

        private void takeFocus()
        {
            // because we are setting WS_EX_NOACTIVATE, popups won't go away when clicked outside, since they are not losing focus (they never got it). calling this fixes that.
            NativeMethods.SetForegroundWindow(helper.Handle);
        }

        private void btnTaskList_Click(object sender, RoutedEventArgs e)
        {
            takeFocus();
        }

        private void TaskButton_MouseRightButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            takeFocus();
        }

        private void quickLaunchList_Drop(object sender, DragEventArgs e)
        {
            string[] fileNames = e.Data.GetData(DataFormats.FileDrop) as string[];
            if (fileNames != null)
            {
                appGrabber.AddByPath(fileNames, 3);
            }

            e.Handled = true;
        }

        private void quickLaunchList_DragEnter(object sender, DragEventArgs e)
        {
            String[] formats = e.Data.GetFormats(true);
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effects = DragDropEffects.Copy;
            }

            e.Handled = true;
        }

        private void btnDesktopOverlay_Click(object sender, RoutedEventArgs e)
        {
            if (Startup.DesktopWindow != null)
                Startup.DesktopWindow.IsOverlayOpen = (bool)(sender as ToggleButton).IsChecked;
        }

        private void SysSleep(object sender, RoutedEventArgs e)
        {
            //Cairo.ShowSleepConfirmation();
        }

        private void searchStr_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Return)
            {
                
            }
        }

        private void Programs_Drop(object sender, DragEventArgs e)
        {
            string[] fileNames = e.Data.GetData(DataFormats.FileDrop) as string[];
            if (fileNames != null)
            {
                appGrabber.AddByPath(fileNames, 2);
            }
        }

        private void OpenTimeDateCPL(object sender, RoutedEventArgs e)
        {
            Shell.StartProcess("timedate.cpl");
        }

        private void CheckForUpdates(object sender, RoutedEventArgs e)
        {
            WinSparkle.win_sparkle_check_update_with_ui();
        }

        private void OpenLogoffBox(object sender, RoutedEventArgs e)
        {
            //Cairo.ShowLogOffConfirmation();
        }

        private void OpenRebootBox(object sender, RoutedEventArgs e)
        {
            //Cairo.ShowRebootConfirmation();
        }

        private void OpenShutDownBox(object sender, RoutedEventArgs e)
        {
            //Cairo.ShowShutdownConfirmation();
        }

        private void OpenRunWindow(object sender, RoutedEventArgs e)
        {
            Shell.ShowRunDialog();
        }

        bool test = false;
        StartMenu sm = new StartMenu();
        private void ToggleProgramsMenu()
        {
            if (test == true)
            {
                sm.Hide();
                test = false;
            }
            else
            {
                sm.Show();
                test = true;
            }
        }

        private void MenuIcon_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            ToggleProgramsMenu();
        }

        bool show = false;
        Volume v = new Volume();
        private void ImgOpenVolume_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (show == false)
            {
                v.Show();
                show = true;
            }
            else
            {
                v.Hide();
                show = false;
            }

        }

        public void FocusSearchBox(object sender, RoutedEventArgs e)
        {

        }

        private void btnViewResults_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnClearSearch_Click(object sender, RoutedEventArgs e)
        {

        }

        private void OpenTaskManager(object sender, RoutedEventArgs e)
        {
            Shell.StartTaskManager();
        }

        private void OpenCloseCairoBox(object sender, RoutedEventArgs e)
        {
        }

        private void miClock_SubmenuOpened(object sender, RoutedEventArgs e)
        {
            //monthCalendar.DisplayDate = DateTime.Now;
        }

        private void InitCairoSettingsWindow(object sender, RoutedEventArgs e)
        {
            //CairoSettingsWindow window = new CairoSettingsWindow();
            //window.Show();
        }

        private void AboutCairo(object sender, RoutedEventArgs e)
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            //FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            //string version = fvi.FileVersion;

            //CairoMessage.ShowAlert(
             //   Localization.DisplayString.sAbout_Version + " " + version + " - " + Localization.DisplayString.sAbout_PreRelease
              //  + "\n\n" + String.Format("", DateTime.Now.Year.ToString()), "AxOS Desktop Environment", MessageBoxImage.None);
        }
    }
}
