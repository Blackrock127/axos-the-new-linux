﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CairoExplorer
{
    /// <summary>
    /// Interaction logic for Message.xaml
    /// </summary>
    /// 
    public partial class Message : Window
    {
        string type1 = null;
        public Message(string title_1, string line_1, string line_2, string type)
        {
            InitializeComponent();

            title.Content = title_1;
            line1.Content = line_1;
            line2.Content = line_2;

            type1 = type;
            if(type == "sleep")
            {
                title.Content = "Put your pc to sleep?";
                
            }

        }

        private void Label_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (e.ChangedButton == MouseButton.Left)
                    this.DragMove();
            }
            catch (Exception eeeeeee)
            {

            }
        }

        [DllImport("PowrProf.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern bool SetSuspendState(bool hiberate, bool forceCritical, bool disableWakeEvent);

        private void Ok_Click(object sender, RoutedEventArgs e)
        {

            if (type1 == "sleep")
            {
                SetSuspendState(false, true, true);
                this.Close();
            }

            if (type1 == "reboot")
            {
                var psi = new ProcessStartInfo("shutdown", "/r /t 0");
                psi.CreateNoWindow = true;
                psi.UseShellExecute = false;
                Process.Start(psi);
                this.Close();
            }

            if (type1 == "shutdown")
            {
                var psi = new ProcessStartInfo("shutdown", "/s /t 0");
                psi.CreateNoWindow = true;
                psi.UseShellExecute = false;
                Process.Start(psi);
                this.Close();
            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
